#ifndef TREEITEM_H
#define TREEITEM_H

#include "hl7schema/src/api.h"
#include <QTreeWidgetItem>

typedef enum TreeItemType {
    SEGMENT=0,
    FIELD,
    TYPE,
    TYPECOMP
} TreeItemType;

class TreeItem : public QTreeWidgetItem
{
public:
    TreeItem();
    explicit TreeItem(int type = Type);
    explicit TreeItem(const QStringList &strings, int type = Type);
    explicit TreeItem(QTreeWidget *treeview, int type = Type);
    TreeItem(QTreeWidget *treeview, const QStringList &strings, int type = Type);
    TreeItem(QTreeWidget *treeview, QTreeWidgetItem *after, int type = Type);
    explicit TreeItem(QTreeWidgetItem *parent, int type = Type);
    TreeItem(QTreeWidgetItem *parent, const QStringList &strings, int type = Type);
    TreeItem(QTreeWidgetItem *parent, QTreeWidgetItem *after, int type = Type);
    TreeItem(const QTreeWidgetItem &other);

    TreeItemType itemType = SEGMENT;
    Segment *segment = nullptr;
    Field *field = nullptr;
    DataType *dataType = nullptr;
    DataTypeComponent *dataTypeComponent = nullptr;
};

#endif // TREEITEM_H
