#include "treeitem.h"

TreeItem::TreeItem()
    : QTreeWidgetItem() {}

TreeItem::TreeItem(int type)
    : QTreeWidgetItem(type) {}

TreeItem::TreeItem(const QStringList &strings, int type)
    : QTreeWidgetItem(strings, type) {}

TreeItem::TreeItem(QTreeWidget *treeview, int type)
    : QTreeWidgetItem(treeview, type) {}

TreeItem::TreeItem(QTreeWidget *treeview, const QStringList &strings, int type)
    : QTreeWidgetItem(treeview, strings, type) {}

TreeItem::TreeItem(QTreeWidget *treeview, QTreeWidgetItem *after, int type)
    : QTreeWidgetItem(treeview, after, type) {}

TreeItem::TreeItem(QTreeWidgetItem *parent, int type)
    : QTreeWidgetItem(parent, type) {}

TreeItem::TreeItem(QTreeWidgetItem *parent, const QStringList &strings, int type)
    : QTreeWidgetItem(parent, strings, type) {}

TreeItem::TreeItem(QTreeWidgetItem *parent, QTreeWidgetItem *after, int type)
    : QTreeWidgetItem(parent, after, type) {}

TreeItem::TreeItem(const QTreeWidgetItem &other)
    : QTreeWidgetItem(other) {}
