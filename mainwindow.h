#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidgetItem>
#include "treeitem.h"
#include "hl7schema/src/api.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_version_activated(const QString &arg1);

    void on_treeWidget_itemActivated(QTreeWidgetItem *item, int column);

    //void on_treeWidget_itemEntered(QTreeWidgetItem *item, int column);

    void on_treeWidget_itemSelectionChanged();

private:
    Ui::MainWindow *ui;
    hl7type_handle* selected_version = nullptr;
    Segment *get_segment(const char *id);

    TreeItem *createListSegment(Segment *seg);
    TreeItem *createChildren(TreeItem *parent);

};

#endif // MAINWINDOW_H
