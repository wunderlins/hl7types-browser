#include <QDebug>
#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // generate a list of versions
    QStringList hl7versions = {};
    //ui->version->addItem("");
    hl7type_handle *v = version;
    while (v != NULL) {
        hl7versions.append(QString(v->version));
        ui->version->addItem(v->version, v->id);
        v = v->next;
    }

    on_version_activated(version->version);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_version_activated(const QString &arg1)
{
    qDebug() << "version_activated: " << arg1;

    // reset tree selection
    static_cast<QTreeView*>(ui->treeWidget)->selectionModel()->clearSelection();
    ui->treeWidget->blockSignals(true);

    // get version
    selected_version = get_version_by_str(arg1.toStdString().c_str());
    if (selected_version == NULL) // unsupported
        return;

    ui->treeWidget->clear();
    ui->treeWidget->setColumnCount(4);
    ui->treeWidget->setHeaderLabels(QStringList({"Name", "Type", "Table", "Description"}));

    SegmentList *seg = (SegmentList*) selected_version->get_seg();

    QList<QTreeWidgetItem *> items;
    for (int i = 0; i < seg->length; ++i) {
        /*
        QStringList cols = QStringList();
        cols.append(seg->items[i].id);
        cols.append(QString::number(seg->items[i].childCount));
        cols.append(seg->items[i].name);
        TreeItem *t = new TreeItem(static_cast<QTreeWidget *>(nullptr), cols);
        t->itemType = SEGMENT;
        t->segment = &seg->items[i];
        */
        TreeItem *t = createListSegment(&seg->items[i]);
        items.append((QTreeWidgetItem*) t);
    }
    ui->treeWidget->insertTopLevelItems(0, items);

    // re-enable events on tree
    ui->treeWidget->blockSignals(false);


    /*
    // remove all entries from segment list
    ui->segment->clear();



    SegmentList *seg = (SegmentList*) selected_version->get_seg();
    for (int i = 0; i < seg->length; ++i) {
        //items.append(new QTreeWidgetItem(static_cast<QTreeWidget *>(nullptr), QStringList(QString("Segment: %1").arg(seg->items[i].id))));
        ui->segment->addItem(seg->items[i].id);
    }
    */
}

TreeItem *MainWindow::createListSegment(Segment *seg) {
    QStringList cols = QStringList();
    cols.append(seg->id);
    cols.append(QString::number(seg->childCount));
    cols.append("");
    cols.append(seg->name);

    TreeItem *t = new TreeItem(static_cast<QTreeWidget *>(nullptr), cols);
    t->itemType = SEGMENT;
    t->segment = seg;

    // generate children
    createChildren(t);

    return t;
}

TreeItem *MainWindow::createChildren(TreeItem *parent) {
    //TreeItem *t = new TreeItem(parent);

    if (parent->itemType == SEGMENT) {
        Segment *s = parent->segment;
        for (int i=0; i<s->childCount; i++) {
            Field *f = &s->fields[i];
            QStringList cols = QStringList();
            cols.append(QString(s->id) + "-" + QString::number(f->pos));
            cols.append(f->type->id);
            cols.append(f->table);
            cols.append(f->name);

            TreeItem *t = new TreeItem(parent, cols);
            t->itemType = FIELD;
            t->field = f;
            t->segment = s;

            createChildren(t);
        }
    }

    if (parent->itemType == FIELD) {
        DataType *dt = parent->field->type;
        for (int i=0; i<dt->childCount; i++) {
            DataTypeComponent *dtc = &dt->component[i];
            QStringList cols = QStringList();
            cols.append(QString(parent->segment->id) + "-" + QString::number(parent->field->pos) + "." + QString::number(dtc->pos));
            //cols.append(dtc->description);
            cols.append(dtc->type->id);
            cols.append(dtc->table);
            cols.append(dtc->name);

            TreeItem *t = new TreeItem(parent, cols);
            t->itemType = TYPECOMP;
            t->dataTypeComponent = dtc;

            if (dt->childCount)
                createChildren(t);
        }
    }

    if (parent->itemType == TYPECOMP) {
        DataType *dt = parent->dataTypeComponent->type;
        for (int i=0; i<dt->childCount; i++) {
            DataTypeComponent *dtc = &dt->component[i];
            QStringList cols = QStringList();
            cols.append(QString::number(dtc->pos));
            //cols.append(dtc->description);
            cols.append(dtc->type->id);
            cols.append(dtc->table);
            cols.append(dtc->name);

            TreeItem *t = new TreeItem(parent, cols);
            t->itemType = TYPECOMP;
            t->dataTypeComponent = dtc;

            if (dt->childCount)
                createChildren(t);
        }
    }

    return parent;
}

Segment *MainWindow::get_segment(const char *id) {
    SegmentList *seg = (SegmentList*) selected_version->get_seg();
    for (int i=0; i<seg->length; i++)
        if (strcmp(seg->items[i].id, id) == 0)
            return &seg->items[i];
    return NULL;
}

void MainWindow::on_treeWidget_itemActivated(QTreeWidgetItem *item, int column)
{
    qDebug() << "activated " << item->text(0);
}

/*
void MainWindow::on_treeWidget_itemEntered(QTreeWidgetItem *item, int column)
{
    qDebug() << "entered " << item->text(0);
}
*/

void MainWindow::on_treeWidget_itemSelectionChanged()
{
    QList<QTreeWidgetItem *> sel = ui->treeWidget->selectedItems();

    if (sel.length() < 1)
        return;

    TreeItem *t = (TreeItem*) sel.at(0);
    if (t->itemType == SEGMENT)
        qDebug() << "selection " << t->segment->id;
    
}
